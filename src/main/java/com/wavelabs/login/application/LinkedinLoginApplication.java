package com.wavelabs.login.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.wavelabs.login.user.User;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableAutoConfiguration
@ComponentScan({ "com.wavelabs.login.controller","com.wavelabs.login.services","com.wavelabs.login.service.impl","com.wavelabs.login.user" })
@EntityScan("com.wavelabs.login.user")
@EnableJpaRepositories({"com.wavelabs.login.repositories"})

@SpringBootApplication
public class LinkedinLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinkedinLoginApplication.class, args);
	}
	@Value("${accessTokenUrl}")
	private String accessTokenUrl;
	
	@Value("${redirectUrl}")
	private String redirectUrl;
	@Value("${clientId}")
	private String clientId;
	@Value("${clientSecret}")
	private String clientSecret;
	@Value("${grantType}")
	private String grantType;
	@Value("${profileUrl}")
	private String profileUrl;
	
	
	@Bean
	public User user() {
		return new User();
	}
	
	
}
