package com.wavelabs.login.controller;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.wavelabs.login.repositories.UserRepository;
import com.wavelabs.login.user.User;

import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
public class LoginController {
	static final Logger log = Logger.getLogger(LoginController.class);
	@Autowired
	private Environment env;
	@Autowired(required = true)
	private UserRepository userRepo;

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET, value = "/authcode")
	public ResponseEntity getAuthCode(@RequestParam String code) throws JsonProcessingException {
		String jObjAccessToken = getAccessToken(code);
		String profile = getProfile(jObjAccessToken);
		RestMessage restMessage = new RestMessage();
		if (profile != null) {
			restMessage.message = "Profile retrieval success!";
			restMessage.messageCode = "200";

			return ResponseEntity.status(200).body(restMessage);
		} else {
			restMessage.message = "Retrieval of Profile Fails!";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}

	}

	@SuppressWarnings("rawtypes")
	private String getProfile(String jObjAccessToken) throws JsonProcessingException {
		@SuppressWarnings("unused")
		String profile = "";
		String uri = env.getProperty("profileUrl");
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.set("Connection", "Keep-Alive");
		requestHeaders.set("Authorization", "Bearer " + jObjAccessToken);
		System.out.println(jObjAccessToken);
		requestHeaders.set("Host", "api.linkedin.com");
		HttpEntity<String> entity = new HttpEntity<>(requestHeaders);
		System.out.println(entity);
		ResponseEntity response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
		System.out.println(response);
		JSONParser jParser = new JSONParser();
		String jObjUserProfile = "";
		try {
			JSONObject jObject = (JSONObject) (jParser.parse("response"));
			System.out.println(jObject);
			jObjUserProfile = (String) jObject.get("emailAddress");
			System.out.println(jObjUserProfile);
			// if email doesn't exists in db save the user object in db
			User user = userRepo.findByEmail("email");
			String email = user.getEmail();
			String firstname = user.getFirstname();
			String lastname = user.getLastname();
			String image = user.getImage();

			if (email.equals(jObjUserProfile)) {
				User newUser = new User();
				user.setEmail(jObjUserProfile);
				user.setFirstname(firstname);
				user.setLastname(lastname);
				user.setImage(image);
				userRepo.save(newUser);
			}
		} catch (ParseException e) {
			log.error(e);
		}
		return profile = (String) response.getBody();

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET, value = "/loginstatus")
	public ResponseEntity getLoginStatus() {
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();

	}

	@SuppressWarnings("rawtypes")
	public String getAccessToken(String code) {
		// Create the request body as a MultiValueMap
		MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
		final String uri = env.getProperty("accessTokenUrl");
		// final String uri = "https://www.linkedin.com/oauth/v2/accessToken";
		body.add("grant_type", env.getProperty("grantType"));
		body.add("code", code);
		body.add("redirect_uri", env.getProperty("redirectUrl"));
		body.add("client_id", env.getProperty("clientId"));
		body.add("client_secret", env.getProperty("clientSecret"));
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity entity = new HttpEntity<MultiValueMap<String, String>>(body, requestHeaders);
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		ResponseEntity response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		String accessToken = (String) response.getBody();
		System.out.println(accessToken);

		JSONParser jParser = new JSONParser();
		String jObjAccessToken = "";
		try {
			JSONObject jObject = (JSONObject) (jParser.parse(accessToken));
			System.out.println(jObject);
			jObjAccessToken = (String) jObject.get("access_token");
			System.out.println(jObjAccessToken);
		} catch (ParseException e) {
			log.error(e);
		}

		return jObjAccessToken;
	}
}
